"""
Parses blazon input into XML object that
can be interpreted and used to generate
an image representing the blazon.
"""