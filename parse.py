import re
from itertools import chain
from tokenizer import tokenize
from lexicon import color_parse, division_parse, charge_parse, division_parse

class UnexpectedEndOfStream(Exception): pass
class InvalidTokenPassed(Exception): pass
class UnknownTokenPassed(Exception): pass

def parse(s):
    tokens = tokenize(s)
    current = None
    ahead = []

    def peek(n):
        # Helper function; peeks n elements ahead in generator
        nonlocal tokens
        buffer = []

        for _ in range(0, n):
            buffer.append(next(tokens, None))
        
        tokens = chain(buffer, tokens)

        return buffer
    
    def advance(n = 1):
        # Helper function; advances generator n times
        nonlocal current, ahead

        for _ in range(0, n):
            current = next(tokens, None)
        
        ahead = peek(2)

        return False if current == None else True
    
    def parse_blazon():
        tree = []

        while not peek(1)[0] == None:
            advance()
            tree.append(parse_statement())
        
        return {
            'type': 'blazon',
            'value': tree
        }
    
    def parse_statement():
        if current.type in ['PER', 'TIERCED', 'QUARTERLY']:
            return parse_division()
        
        if current.type in ['DIVISION', 'CHARGE']:
            return parse_element()
        
        if current.type == 'NUMBER':
            if ahead[0] and ahead[0].type == 'CHARGE':
                num = current.value
                advance()
                return parse_element(num)
            return parse_number()
        
        if current.type == 'POSITION':
            return parse_position()
        
        if current.type == 'COLOR':
            return parse_color()
        
        if current.type == 'MODIFIER':
            return parse_modifier()
            
        if current.type == 'WORD':
            if current.value.upper() in ['PARTY', 'PARTED']:
                advance()
                return parse_statement()
            else:
                raise UnknownTokenPassed(current) # WORD tokens are a catch-all; not supposed to be parsable
        
        if current.type in ['IN', 'OF', 'BEGIN']:
            advance()
            return parse_statement()

        raise InvalidTokenPassed(current)

    def parse_number():
        return {
            'type': 'number',
            'value': current.value
        }
    
    def parse_color():
        return {
            'type': 'color',
            'value': color_parse(current.value) # french -> modern english
        }
    
    def parse_modifier():
        return {
            'type': 'modifier',
            'value': current.value
        }
    
    def parse_position():
        print(current)
        return {
            'type': 'position',
            'value': current.value
        }
    
    def parse_colors(n = 1):
        return parse_multiple(parse_color, 'color', n)
    
    def parse_positions(n = 1):
        return parse_multiple(parse_position, 'position', n)

    def parse_multiple(func, typ, n):
        found = []

        for _ in range(0, n):
            if ahead[0].type == typ.upper():
                advance()
                found.append(func())
            else:
                raise InvalidTokenPassed()

        return found

    def parse_element(num = None):
        # A charge or an ordinary
        node = {}
        properties = []

        if current.type == 'DIVISION':
            node['type'] = 'ordinary'
            node['ordinate'] = division_parse(current.value)
        elif current.type == 'CHARGE':
            node['type'] = 'charge'
            node['charge'] = charge_parse(current.value)

            if num:
                node['number'] = num

        while ahead[0]:
            if ahead[0].type == 'COLOR':
                node['color'] = parse_colors() # parse colors (n = 1) advances by 1
                continue
            
            if ahead[0].type in ['PER', 'TIERCED']:
                advance()
                node['division'] = parse_division()
                continue

            if ahead[0].type == 'MODIFIER':
                advance()
                properties.append(parse_modifier())
                continue
                
            if ahead[0].type in ['IN', 'OF']:
                if not ahead[1]:
                    continue
                    
                if ahead[1].type in ['POSITION', 'DIVISION']:
                    if not ahead[1]:
                        raise UnexpectedEndOfStream()
                    if ahead[1].type == 'POSITION':
                        advance(2)
                        properties.append(parse_position())

                        continue
            
            if ahead[0].type == 'DIVISION' and division_parse(ahead[0].value).upper() == 'FESS':
                if ahead[1].type == 'POSITION' and ahead[1].value.upper() == 'POINT':
                    advance(2)
                    properties.append({
                        'type': 'position',
                        'value': 'fess'
                    })
                    properties.append(parse_position())
                    
                    continue
            
            if node['type'] == 'charge':
                if ahead[0].type == 'NUMBER':
                    advance()
                    properties.append(parse_number())

                    continue
                
                if ahead[0].type == 'OF':
                    if ahead[1]:
                        if ahead[1].type == 'NUMBER':
                            advance(2)
                            properties.append(parse_number())

            break
                    
        
        if len(properties) > 0:
            node['properties'] = properties
        
        return node

    def parse_division():
        node = {'type': 'division'}
        steps_ahead = 0

        if current.type == 'QUARTERLY':
            return parse_quarterly()

        if current.type == 'TIERCED':
            steps_ahead = 1

            if not ahead[0] or not ahead[1]:
                raise UnexpectedEndOfStream()

            if not ahead[0].type in ['PER', 'IN']:
                raise InvalidTokenPassed()
            
            if not ahead[1].type == 'DIVISION':
                raise InvalidTokenPassed()
            
            node['division'] = 'tierced'
        elif current.type == 'PER':
            steps_ahead = 0

            if not ahead[0]:
                raise UnexpectedEndOfStream()
            
            if not ahead[0].type == 'DIVISION':
                raise InvalidTokenPassed()
            
            node['division'] = 'per'

        node['ordinate'] = division_parse(ahead[steps_ahead].value)
        
        if not advance(1 + steps_ahead):
            print(current)
            raise UnexpectedEndOfStream()
        
        node['color'] = parse_colors(2 + steps_ahead)

        return node
    
    def parse_quarterly():
        # TODO: Implement quarterly
        return {
            'type': 'division',
            'division': 'quarterly'
        }

    return parse_blazon()