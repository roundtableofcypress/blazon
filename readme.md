# Blazon
A program that converts a blazon into an image.

## What is a blazon?
per Wikipedia:
> In heraldry and heraldic vexillology, a blazon is a formal description of a coat of arms, flag or similar emblem, from which the reader can reconstruct the appropriate image. The verb *to blazon* means to create such a description. The visiual depiction of a coat of arms or flag has traditionally had considerable latitude in design, but a verbal blazon specifies the essentially distinctive elements.

## Hasn't this been done before? (and credit)
Yes, this sort of project has been done by Luke Torjussen as his [final year project](http://studentnet.cs.manchester.ac.uk/resources/library/3rd-year-projects/2012/Luke.Torjussen.pdf) at the University of Manchester, by the authors of [pyBlazon](http://web.meson.org/pyBlazon/), and as a passion project by Karl Wilcox of drawshield.net

This project was inspired by the three aforementioned projects, and admittedly its functionality is markedly similar to these projects. It is similar in that this project is essentially a blazon compiler, treating blazon similarly to a programming language and instead of machine code output, SVG code to be rendered in browser (or as a PNG).

## What is this?
>This project is very much in its infancy, and currently there is no official specification of the project's intended functionality has been developed, reviewed nor approved.
A blazon is a statement written in a language that describes a heraldic emblem which can be used to reconstruct an image, as a result, it can be treated as a sort of programming language. Blazon is a written language with its own lexicon, grammar and syntax; it was not a language deliberately created for programming, rather for human interpretation. Compared to other spoken/written languages like English, Blazon is very rigid for such a language. As a programming language, Blazon is very loosely typed, and lacks expressions, variables, functions, arithmetic, mathematic operations and most other functionalities present in other programming languages. However, one feature that is prominent in Blazon and in essence, the language, are statements.