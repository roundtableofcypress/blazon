from lexicon import colors_regex, divisions_regex, charges_regex, modifiers_regex, positions_regex

keywords = [
    'PER', 'TIERCED', 'QUARTERLY',
    'IN', 'OF'
]

tokens = [
    ('POSITION', positions_regex),
    ('COLOR', colors_regex),
    ('DIVISION', divisions_regex),
    ('CHARGE', charges_regex),
    ('MODIFIER', modifiers_regex),
    ('NUMBER', r'\d+'),
    ('WORD', r'\w+'),
    ('COMMENT', r'\#[^\r\n]*'),
    ('LINECOMMENT', r'\[.*?\]'),
    ('NEWLINE', r'\n'),
    ('WHITESPACE', r'[ \t]'),
    ('PUNCTUATION', r'[\\\.,\/#!$%\^&\*;:{}=\-_`~()]'),
    ('MISMATCH', r'.')
]
