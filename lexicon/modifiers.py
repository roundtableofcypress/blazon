import re
from lexicon.util import Modifier
from lexicon import charges
modifiers_regex = []
modifiers_regex2 = []

modifiers = [
    Modifier('much', r'much'),
    Modifier('smaller', r'smaller'),
    Modifier('larger', r'larger'),
    Modifier('inverted', r'inverted')
]

for charge in charges:
    for modifier in charge.modifiers:
        modifiers.append(modifier)

for modifier in modifiers:
    modifiers_regex.append(modifier.regex)
    modifiers_regex2.append(modifier.regex2)

modifiers_regex = '(' + '|'.join(modifiers_regex) + ')'
modifiers_regex2 = '|'.join(modifiers_regex2)

def modifier_parse(s):
    return re.search(modifiers_regex2, s).lastgroup