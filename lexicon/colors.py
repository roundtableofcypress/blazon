import re

colors = [
    ('blue', r'az(ure)?'),
    ('skyblue', r'bleu celeste'),
    ('red', r'gui?(les)?'),
    ('mulberry', r'murrey'),
    ('purple', r'purp(ure)?'),
    ('black', r'sa(ble)?'),
    ('bloodred', r'sanguine'),
    ('orange', r'(tenne|tanned|tawny)'),
    ('green', r'vert'),
    ('gold', r'or'),
    ('silver', r'ar(gent)?')
]

colors_regex = []
for color in colors:
    colors_regex.append(color[1])

colors_regex = '(' + '|'.join(colors_regex) + ')'
colors_regex2 = '|'.join('(?P<%s>%s)' % pair for pair in colors)

def color_parse(s):
    return re.search(colors_regex2, s).lastgroup