import re

divisions = [
    ('pale', r'pal(e|y)'),
    ('fess', r'fesse?'),
    ('bend', r'bendy?'),
    ('pile', r'pile'),
    ('chevron', r'chevron'),
    ('saltire', r'saltire'),
    ('gyronny', r'gyronny'),
    ('pall', r'(pall|pairle)'),
    ('barry', r'barry'),
    ('cross', r'cross')
]

divisions_regex = []
for division in divisions:
    divisions_regex.append(division[1])

divisions_regex = '(' + '|'.join(divisions_regex) + ')'
divisions_regex2 = '|'.join('(?P<%s>%s)' % pair for pair in divisions)

def division_parse(s):
    return re.search(divisions_regex2, s).lastgroup