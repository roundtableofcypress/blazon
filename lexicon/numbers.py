import re

numbers = {
    'one': 1,
    'two': 2,
    'three': 3,
    'four': 4,
    'five': 5,
    'six': 6,
    'seven': 7,
    'eight': 8,
    'nine': 9,
    'ten': 10,
    'eleven': 11,
    'twelve': 12,
    'thirteen': 13,
    'fourteen': 14,
    'fifteen': 15,
    'sixteen': 16,
    'seventeen': 17,
    'eighteen': 18,
    'nineteen': 19,
    'twenty': 20,
    'thirty': 30
}

def number_parse(s):
    prev = None
    
    if type(s) == int:
        return s
    
    try:
        for w in re.finditer(r'\w+', s):
            v = numbers[w.group(0).lower()]

            if prev == None:
                prev = v
            elif prev > v:
                prev += v
            else:
                prev *= v
        
        return prev
    except Exception:
        return None