import re
from lexicon.util import Charge, Modifier

charges_regex = []
charges_regex2 = []

charges = [
    Charge('roundel', r'roundels?'),
    Charge('annulet', r'annulets?'),
    Charge('mullet', r'mullets?'),
    Charge('fusil', r'fusils?'),
    Charge('billet', r'billets?'),
    Charge('mascle', r'mascles?'),
    Charge('eagle', r'eagles?', [
        Modifier('double headed', r'double headed'),
        Modifier('recursant', r'recursant'),
        Modifier('full', r'full'),
        Modifier('aspect', r'aspect'),
        Modifier('overature', r'overature|close'),
        Modifier('addorsed', r'addorsed')
        # TODO: add other modifiers
    ]),
    Charge('lion', r'lions?', [
        Modifier('sejant erect', r'erect'),
        Modifier('rampant', r'rampant'),
        Modifier('passant', r'passant'),
        Modifier('statant', r'statant'),
        Modifier('salient', r'salient'),
        Modifier('sejant', r'sejant'),
        Modifier('couchant', r'couchant'),
        Modifier('dormant', r'dormant')
        # TODO: add other modifiers, from Wikipedia: guardant, regardant, coward, with forked tail, with crossed tail, with crossed tail (reverse), with tail nowed
    ])
]

for charge in charges:
    charges_regex.append(charge.regex)
    charges_regex2.append(charge.regex2)

charges_regex = '(' + '|'.join(charges_regex) + ')'
charges_regex2 = '|'.join(charges_regex2)

def charge_parse(s):
    return re.search(charges_regex2, s).lastgroup