import re

positions = [
    ('fess point', r'fess point'),
    ('honour', r'honou?r'),
    ('chief', r'chief'),
    ('base', r'base'),
    ('middle', r'middle'),
    ('dexter', r'dexter'),
    ('sinister', r'sinister'),
    ('point', r'point'),
    ('navel', r'(navel|nombril)'),
]

positions_regex = []
for position in positions:
    positions_regex.append(position[1])

positions_regex = '(' + '|'.join(positions_regex) + ')'
positions_regex2 = '|'.join('(?P<%s>%s)' % pair for pair in positions)

def position_parse(s):
    return re.search(positions_regex2, s).lastgroup