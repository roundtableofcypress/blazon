from lexicon.colors import colors, colors_regex, colors_regex2, color_parse
from lexicon.divisions import divisions, divisions_regex, divisions_regex2, division_parse
from lexicon.charges import charges, charges_regex, charges_regex2, charge_parse
from lexicon.modifiers import modifiers, modifiers_regex, modifiers_regex2, modifier_parse
from lexicon.positions import positions, positions_regex, positions_regex2, position_parse
from lexicon.numbers import number_parse