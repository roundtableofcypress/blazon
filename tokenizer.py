import collections, re
from lexicon import number_parse
from tokens import keywords, tokens
Token = collections.namedtuple('Token', ['type', 'value', 'ln', 'col'])

token_regex = '|'.join('(?P<%s>%s)' % pair for pair in tokens)

def preprocess(s):
    # ('COMMENT', r'\#[^\r\n]*')
    # ('LINECOMMENT', r'\[.*?\]')
    s = re.sub(r'\#[^\r\n]*', '', s)
    s = re.sub(r'\[.*?\]', '', s)

    return s

def tokenize(s):
    s = preprocess(s)
    line = 1
    line_start = 0

    for match in re.finditer(token_regex, s):
        token_type = match.lastgroup
        token_value = match.group(token_type)

        if token_type == 'NEWLINE':
            line_start = match.end()
            line += 1
        else:
            if token_type == 'WORD':
                if token_value.upper() in keywords:
                    token_type = token_value.upper()
                elif token_value.upper() in ['AN', 'A']:
                    token_type = 'BEGIN'
                elif number_parse(token_value) != None:
                    token_type = 'NUMBER'
                    token_value = number_parse(token_value)
                    
            if token_type not in ['WHITESPACE', 'COMMENT', 'LINECOMMENT', 'PUNCTUATION', 'MISMATCH']:
                yield Token(token_type, token_value, line, match.start() - line_start)